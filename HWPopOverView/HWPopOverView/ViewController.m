//
//  ViewController.m
//  HWPopOverView
//
//  Created by 洪伟 on 16/6/19.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import "ViewController.h"
#import "HWPopOverView.h"

@interface ViewController (){
    HWPopOverView *_popOverView;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CGFloat margin = 20;
    CGFloat w = CGRectGetWidth([UIScreen mainScreen].bounds) - 2 * margin;
    HWPopOverView *view = [[HWPopOverView alloc] initWithFrame:(CGRect){margin,0,w,0} titleMenus:@[@"推荐",@"全部",@"关注店"]];
    view.contentColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
    view.selectedIndex = 1;
    view.rowHeight = 40;
    _popOverView = view;
}


- (IBAction)show:(UIButton *)sender {
    
    [_popOverView showFrom:sender animation:YES];
}

@end
