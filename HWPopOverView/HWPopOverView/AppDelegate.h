//
//  AppDelegate.h
//  HWPopOverView
//
//  Created by 洪伟 on 16/6/19.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

