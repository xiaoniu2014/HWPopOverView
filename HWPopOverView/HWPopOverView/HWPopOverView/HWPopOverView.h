//
//  HWPopOverView.h
//  HWPopOverView
//
//  Created by 洪伟 on 16/6/19.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HWPopOverView;
@protocol HWPopOverViewDelegate <NSObject>
@optional
- (void)popOverView:(HWPopOverView *)view didSelectedIndex:(NSInteger)index;

@end




@interface HWPopOverView : UIView

@property (nonatomic,weak) id<HWPopOverViewDelegate> delegate;
/**
 *  当前选择行
 */
@property (assign, nonatomic) NSInteger selectedIndex;
@property (assign, nonatomic) CGFloat rowHeight;
/**
 *  table的颜色
 */
@property (strong, nonatomic) UIColor *contentColor;
- (instancetype)initWithFrame:(CGRect)frame titleMenus:(NSArray *)titles;
- (void)showFrom:(UIView *)fromView animation:(BOOL)animation;

@end
