//
//  HWPopOverView.m
//  HWPopOverView
//
//  Created by 洪伟 on 16/6/19.
//  Copyright © 2016年 hongw. All rights reserved.
//


#import "HWPopOverView.h"

@interface HWPopOverView ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *table;
@property (nonatomic, strong) NSArray *titleMenus;
@property (assign, nonatomic) CGRect tableFrame;
@end
static CGFloat kAnimationDuration = 0.25f;
static CGFloat kTextFont = 13.0f;
static NSString *kIdentifier = @"HWPopOverCell";

@implementation HWPopOverView

#pragma mark - Life Cycle
- (instancetype)initWithFrame:(CGRect)frame titleMenus:(NSArray *)titles{
    self = [super initWithFrame:frame];
    if (self) {
        _titleMenus = titles;
        _tableFrame = frame;
//        默认行高44
        _rowHeight = 44.0f;
        [self addSubview:self.table];
        
    }
    return self;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self dismiss];
}

#pragma mark - Private Method
- (void)showFrom:(UIView *)fromView animation:(BOOL)animation{
    UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
    [window addSubview:self];
    self.frame = window.bounds;
    
    CGRect newFrame = [fromView.superview convertRect:fromView.frame toView:window];
    CGFloat y = CGRectGetMaxY(newFrame);
    CGFloat h = _titleMenus.count * _rowHeight;
    
    _tableFrame.origin.y = y;
    _tableFrame.size.height = 0;
    _table.frame = _tableFrame;
    
    
    
    [UIView animateWithDuration:kAnimationDuration animations:^{
        _tableFrame.size.height = h;
        _table.frame = _tableFrame;
    }];
    
}

- (void)dismiss{
    CGRect frame = _table.frame;
    frame.size.height = 0;
    [UIView animateWithDuration:kAnimationDuration animations:^{
        _table.frame = frame;
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _titleMenus.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = _titleMenus[indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.font = [UIFont systemFontOfSize:kTextFont];
    UIColor *textColor = [UIColor whiteColor];
    if (indexPath.row == _selectedIndex) {
        textColor = [UIColor redColor];
    }
    cell.textLabel.textColor = textColor;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedIndex = indexPath.row;
    [tableView reloadData];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self dismiss];
    });
    if ([self.delegate respondsToSelector:@selector(popOverView:didSelectedIndex:)]) {
        [self.delegate popOverView:self didSelectedIndex:indexPath.row];
    }
}

#pragma mark - Setter/Getter
- (UITableView *)table{
    if (!_table) {
        _table = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _table.delegate = self;
        _table.dataSource = self;
        _table.scrollEnabled = NO;
        [_table registerClass:[UITableViewCell class] forCellReuseIdentifier:kIdentifier];
        if ([_table respondsToSelector:@selector(setSeparatorInset:)]) {
            [_table setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([_table respondsToSelector:@selector(setLayoutMargins:)]) {
            [_table setLayoutMargins:UIEdgeInsetsZero];
        }
    }
    return _table;
}

- (void)setContentColor:(UIColor *)contentColor{
    if (_contentColor != contentColor) {
        _contentColor = contentColor;
        _table.backgroundColor = contentColor;
    }
}

- (void)setRowHeight:(CGFloat)rowHeight{
    if (_rowHeight != rowHeight) {
        _rowHeight = rowHeight;
        _table.rowHeight = rowHeight;
        [_table reloadData];
    }
}


@end
